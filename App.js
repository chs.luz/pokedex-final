import React from 'react';
import Routes from './src/routes';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import reducers from './src/Store/Reducers';

export default function App() {
  return (
    <Provider store={createStore(reducers,{},applyMiddleware(ReduxThunk))}>
         <Routes />
    </Provider>
  );
}