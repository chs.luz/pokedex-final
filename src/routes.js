import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PokemonList from './pages/PokemonList';
import PokemonDetail from './pages/PokemonDetail';

const AppStack = createStackNavigator();

function Routes() {
    return(
        <NavigationContainer >
            <AppStack.Navigator initialRouteName="PokemonList">
                <AppStack.Screen options={{headerShown:false}} name="PokemonList" component={PokemonList}/>
                <AppStack.Screen options={{headerTitle:"Detalhes do Pokémon"}} name="PokemonDetail" component={PokemonDetail}/>
            </AppStack.Navigator>
        </NavigationContainer>
    )
}  

export default Routes;