import { LIST_PAGINATED_POKEMONS, LOADING, FIND_POKEMON } from "../types"
import api from '../../services/api';
import axios from 'axios';


const loadEntireObjectList = async (data) => {
    const responseArray  = data.map(async res => {
        let response = await axios.get(res.url);
        return { name: res.name, ...response.data}
    });
    return await Promise.all(responseArray);
}

export const changeLoading = (loading) => {
    return ({
        type: LOADING,
        payload: loading
    })
} 

export const listPokemonsPaginated = (offset) => {
    return async dispatch => {
        let response = await api.get('', {
            params: {offset: offset, limit: 20}
        });
        const responseData = response.data.results;
        const lista = await loadEntireObjectList(responseData);
        dispatch ({
            type: LIST_PAGINATED_POKEMONS,
            payload: {list: lista, offset: offset+20, total: response.data.count}
        })
    }
}

export const findPokemon = (text) => {
    return async dispatch => {
        const list = [];
        try{
            let response = await api.get(text);
            const responseData = response.data;
            if(responseData) {
                list.push(responseData);
            }
        }catch {
            console.log('não encontrado');
        }
        dispatch ({
            type: FIND_POKEMON,
            payload: list
        })
    }
}