import { combineReducers } from 'redux';
import Pokemons from './Pokemons';

const rootReducer = combineReducers({
    Pokemons
})

export default rootReducer;