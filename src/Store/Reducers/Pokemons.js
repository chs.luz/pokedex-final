import { LIST_PAGINATED_POKEMONS, LOADING, FIND_POKEMON } from "../types"

const initialState = {
    list: [],
    offset: 0,
    limit: 20,
    total: 0,
    loading: false
}

export default function(state = initialState ,action) {
    switch(action.type) {
        case LIST_PAGINATED_POKEMONS :
            {
                const newLista = [...state.list, ...action.payload.list];
                return{ ...state,list:  newLista, offset:  action.payload.offset, total: action.payload.total, loading: false}
            }
        case FIND_POKEMON: 
            return { ...state, list: action.payload, offset: 0, loading:false}   
        case LOADING:
            return { ...state, loading: action.payload}
        default:
            return state;
    }
}