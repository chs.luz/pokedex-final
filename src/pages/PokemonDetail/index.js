import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PokeItem from '../../components/PokItem';

function PokemonDetail({ route, list }) {
    const { pokemonId } = route.params;
    const [pokemon,setPokemon] = useState()
    useEffect(() => {
        const dado = list.filter(pok => pok.id === pokemonId);
        setPokemon(dado[0]);
    }, [pokemonId])
    return (
       pokemon !== undefined && <PokeItem item={pokemon} showFull={true}  />
    )
}

mapStateToProps = (state) => {
    return {
      list: state.Pokemons.list,
    };
  };
  
  mapDispatchToProps = (dispath) => {
    return bindActionCreators({  }, dispath);
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(PokemonDetail);