import styled from 'styled-components/native';
import { FontAwesome } from "@expo/vector-icons";

export const ViewContainer = styled.View `
    marginTop: 25px;
`

export const AppTitle = styled.Text `
    width: 334px;
    height: 38px;
    marginLeft: 40px;
    marginTop: 10px;
    fontStyle: normal;
    fontWeight: bold;
    fontSize: 32px;
    lineHeight: 38px;
`

export const AppPresentation = styled.Text `
    width: 334px;
    height: 38px;
    marginLeft: 40px;
    marginTop: 10px;
    fontStyle: normal;
    fontWeight: normal;
    fontSize: 16px;
    lineHeight: 19px;
    color: #747476;
`

export const ContainerSearch = styled.View `
    flexDirection: row;
    margin: 10px;
    height: 50px;
    justifyContent: center;
    alignItems: center;
    marginVertical: 20px;
`

export const InputSearch = styled.TextInput `
    backgroundColor: #C4C4C4;
    flex: 1;
    height: 60px;
    width: 334px;
    borderRadius: 8px;
    paddingLeft: 40px;
`
export const SearchImage = styled(FontAwesome) `
    position: absolute; 
    left: 10px;
`

export const CardList = styled.FlatList `
`