import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  listPokemonsPaginated,
  changeLoading,
  findPokemon
} from "../../Store/Actions/Pokemons";

import PokItem from "../../components/PokItem";

import {
  ViewContainer,
  AppTitle,
  AppPresentation,
  ContainerSearch,
  InputSearch,
  SearchImage,
  CardList,
} from "./estilo";

function PokemonList(props) {
  const [search, setSearch ] = useState('');


  function loadingList() {
    let total = props.total;
    let offset = props.offset;
    let loading = props.loading;
    if (loading || search.trim() !== '') {
      return;
    }
    if (total > 0 && offset >= total) {
      return;
    }
    props.changeLoading(true);
    props.listPokemonsPaginated(offset);
  }

  const onChangeSearch = (text) => {
    setSearch(text);
    if(text.trim() !== '') {
      props.findPokemon(text.toLowerCase());
    }
  }

  useEffect(() => {
    loadingList();
  }, [search]);

  return (
    <ViewContainer>
      <AppTitle>Pokédex</AppTitle>
      <AppPresentation>
        Search pokémon by name or using the National Pokémon number
      </AppPresentation>
      <ContainerSearch>
        <InputSearch
          value={search}
          onChangeText={onChangeSearch}
          placeholder="What Pokémon are you looking for?"
          placeholderTextColor="#747476"/>
        <SearchImage name="search" size={16} color="black" />
      </ContainerSearch>
      <CardList
        data={props.list}
        keyExtractor={(pokemon) => pokemon.name}
        showsVerticalScrollIndicator={false}
        onEndReached={loadingList}
        onEndReachedThreshold={0.2}
        renderItem={({ item }) => <PokItem item={item} showFull={false} navigation={props.navigation}/>}
      />
    </ViewContainer>
  );
}

mapStateToProps = (state) => {
  return {
    list: state.Pokemons.list,
    offset: state.Pokemons.offset,
    total: state.Pokemons.total,
    loading: state.Pokemons.loading,
  };
};

mapDispatchToProps = (dispath) => {
  return bindActionCreators({ listPokemonsPaginated, changeLoading, findPokemon }, dispath);
};

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);
