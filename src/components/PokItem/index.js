import React from "react";
import { View, Text } from "react-native";
import {
  CardItemView,
  CardContainerDescription,
  CardContainerIdDescription,
  CardContainerNameDescription,
  PokeImage,
  ContainerStats,
  BaseStats,
  HP,
  Attack,
  Defense,
  SpAttack,
  SpDefense,
  Speed,
  Total
} from "./estilo";
import { getColor } from "../../utils";

const PokItem = ({ item, navigation, showFull }) => {
  const navigateToDetail = () => {
    if(navigation)
      navigation.navigate("PokemonDetail", { pokemonId: item.id });
  };

  const getInfo = (text) => {
    let value = 0;
    item.stats.forEach(stat => {
       if(stat.stat.name === text) {
         value = stat.base_stat;
         return value;
       }
    })
    return value;
  } 
 
  const getTotal = () => {
    let value = 0;
    item.stats.forEach(stat => {
      value += stat.base_stat;
    })
    return value;
  }
  return (
    <View>
      <CardItemView color={getColor(item)} onPress={navigateToDetail}>
        <CardContainerDescription>
          <CardContainerIdDescription>#{item.id}</CardContainerIdDescription>
          <CardContainerNameDescription>
            {item.name}
          </CardContainerNameDescription>
        </CardContainerDescription>
        <PokeImage source={{ uri: item.sprites.front_default }} />
      </CardItemView>
      {showFull ? 
        <ContainerStats>
            <BaseStats>Base Stats</BaseStats>
            <HP>Hp: {getInfo('hp')}</HP>
            <Attack>Attack: {getInfo('attack')}</Attack>
            <Defense>Defense: {getInfo('defense')}</Defense>
            <SpAttack>Sp. Attack: {getInfo('special-attack')}</SpAttack>
            <SpDefense>Sp. Defense: {getInfo('special-defense')}</SpDefense>
            <Speed>Speed: {getInfo('speed')}</Speed>
            <Total>Total: {getTotal()}</Total>
        </ContainerStats> : null}
    </View>
  );
};

export default PokItem;
