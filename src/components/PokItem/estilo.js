import styled from 'styled-components/native';

export const CardItemView = styled.TouchableOpacity `
    marginTop: 10px;
    marginHorizontal: 5px;
    height: 115px;
    borderRadius: 6px;
    backgroundColor: ${props => props.color};
    flexDirection: row;
`

export const CardContainerDescription = styled.View `
    flex: 1;
    justifyContent: center; 
`

export const CardContainerIdDescription = styled.Text `
    marginLeft: 20px;
    fontStyle: normal;
    fontWeight: bold,
    fontSize: 16px;
    lineHeight: 18px;
`

export const CardContainerNameDescription = styled.Text `
    marginLeft: 20px;
    fontStyle: normal;
    fontWeight: bold;
    fontSize: 26px;
    lineHeight: 31px;
    color: #FFFFFF;
`

export const PokeImage = styled.Image `
    width: 120px;
    height: 120px;
    flex: 1
`

export const ContainerStats = styled.View `
    height: 400px;
    marginHorizontal: 5px;
`

export const BaseStats = styled.Text `
    color: #62B957;
    lineHeight: 20px;
    fontSize: 18px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const HP = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const Attack = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const Defense = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const SpAttack = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const SpDefense = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const Speed = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`

export const Total = styled.Text `
    lineHeight: 18px;
    fontSize: 16px;
    fontWeight: bold;
    marginTop: 10px;
    marginLeft: 10px;
`