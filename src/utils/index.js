export const getColor = (item) => {
    let types = item.types;
    let color = "gray";
    types.forEach((type) => {
      if (type.type.name === "grass") {
        color = "#62B957";
        return;
      }
      if (type.type.name === "fire") {
        color = "#FD7D24";
        return;
      }
      if (type.type.name === "water") {
        color = "#58ABF6";
        return;
      }
      if (type.type.name === "bug") {
        color = "#8BD674";
        return;
      }
      if (type.type.name === "normal") {
        color = "#B5B9C4";
        return;
      }
      if (type.type.name === "flying") {
        color = "#B5B9C4";
        return;
      }
      if (type.type.name === "poison") {
        color = "#9F6E97";
        return;
      }
      if (type.type.name === "electric") {
        color = "#F2CB55";
        return;
      }
      if (type.type.name === "ground") {
        color = "#F78551";
        return;
      }
      if (type.type.name === "fairy") {
        color = "#EBA8C3";
        return;
      }
      if (type.type.name === "fighting") {
        color = "#EB4971";
        return;
      }
      if (type.type.name === "psychic") {
        color = "gray";
        return;
      }
    });
    return color;
  };
  